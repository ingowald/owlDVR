# ======================================================================== #
# Copyright 2019-2020 Ingo Wald                                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#     http://www.apache.org/licenses/LICENSE-2.0                           #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
# ======================================================================== #

if (POLICY CMP0048)
  cmake_policy(SET CMP0048 NEW)
endif()
if (POLICY CMP0071)
  cmake_policy(SET CMP0071 NEW)
endif()
project(owlDVR VERSION 1.0.4 LANGUAGES C CXX CUDA)
cmake_minimum_required(VERSION 2.8)

option(DUMP_FRAMES "DUMP_FRAMES" OFF)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${owl_dir}/owl/cmake/")

add_subdirectory(submodules/owl EXCLUDE_FROM_ALL)
add_subdirectory(submodules/cuteeOWL EXCLUDE_FROM_ALL)

set(CMAKE_AUTOMOC OFF)
embed_ptx(
  OUTPUT_TARGET
  deviceCode_ptx
  
  PTX_LINK_LIBRARIES
  owl::owl
  qtOWL
  
  SOURCES
  deviceCode.cu
  )

set(CMAKE_AUTOMOC ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_CXX_COMPILE_OPTIONS_PIE "-fPIC")

# ------------------------------------------------------------------
add_executable(owlDVRViewer
  viewer.cpp
  Model.cpp
  Renderer.cpp
  SpaceSkipper.cu
  )

if (DUMP_FRAMES)
  target_add_definitions(owlDVRViewer
    PRIVATE
    -DDUMP_FRAMES=1
    )
endif()

target_link_libraries(owlDVRViewer
  PRIVATE 
  owl::owl 
  deviceCode_ptx
  qtOWL
  )

# ------------------------------------------------------------------
add_executable(dvrSubSampleTool
  dvrSubSample.cpp
  )
target_link_libraries(dvrSubSampleTool
  PRIVATE
  owl::owl
  )