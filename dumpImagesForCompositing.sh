#!/bin/bash
echo "if this doesn't dump images (but keeps the window open), make sure to enable DUMP_FRAMES in the cmake dialog"
for iz in 0 1; do
    for iy in 0 1; do
	for ix in 0 1; do
	    ./owlDVRViewer \
		/space/structured/llnl_0250.raw -dims 2048 2048 1920 -f uchar\
		--camera 1516.49 314.208 -1007.79 1024 1024 959.999 0 1 0 -fovy 70\
		--sub-brick $ix $iy $iz 1024\
		-o "llnl-dump_x"${ix}"_y"${iy}"_z"${iz}
	done
    done
done

	  
for iz in 0 1; do
    for iy in 0 1; do
	for ix in 0 1; do
	    ./owlDVRViewer \
		/space/dns/dns_10240x7680x1536_float.raw -dims 10240 7680 1536 -f float\
		--sub-brick $ix $iy $iz 1024\
		-o "llnl-dump_x"${ix}"_y"${iy}"_z"${iz}
	done
    done
done

	  
