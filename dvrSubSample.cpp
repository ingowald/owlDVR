#include "Model.h"
#include <fstream>

using namespace dvr;

int main(int ac, char **av)
{
  const std::string inFileName = av[1];
  const vec3i inDims(atoi(av[2]),
                     atoi(av[3]),
                     atoi(av[4]));
  const int ssRatio = atoi(av[5]);
  const std::string outFileName = av[6];

  std::ifstream in(inFileName,std::ios::binary);
  std::ofstream out(outFileName,std::ios::binary);
  for (int iz=0;iz<inDims.z;iz++)
    for (int iy=0;iy<inDims.y;iy++)
      for (int ix=0;ix<inDims.x;ix++) {
        float f;
        in.read((char*)&f,sizeof(f));
        if ((ix%ssRatio)==0 &&
            (iy%ssRatio)==0 &&
            (iz%ssRatio)==0) {
          out.write((char*)&f,sizeof(f));
        }
      }
  vec3i outDims = divRoundUp(inDims,vec3i(ssRatio));
  PRINT(outDims);
}
